<?php
/**
 * Created by: Stephan Hoeksema 2022
 * wfflix
 */
require 'database/Connection.php';

return Connection::make();
